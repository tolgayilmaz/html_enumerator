'''

'''
__author__ = 'tolga'

import os, time, sqlite3
from datetime import datetime
from numpy import matrix
from multiprocessing import Pool
from numpy import linalg
from random import randint
from more_itertools import recipes
import numpy as np

class DBReader(object):

    conn = sqlite3.connect('links.db')
    c = conn.cursor()

    def __init__(self):
        self.edge_pair = dict()  # A list of all links from key to other enumerated pages
        # self.edge_pair.setdefault(0, [])

        self.get_edges = "SELECT * FROM edges"
        self.get_edges_by_id = "SELECT dst FROM edges WHERE src=:src_id"
        self.get_paths = "SELECT * FROM paths"
        self.files_count = 11560

        self.path_binary_matrix = matrix('')  # Matrix of links between pages 1 if exists 0 o.w
        self.path_binary_matrix.fill(0)

        # This matrix is for math calculations
        self.path_binary_matrix_salt = matrix('')  # Matrix of links between pages 1 if exists 0 o.w
        self.path_binary_matrix_salt.fill(0)

        self.link_visit_count_matrix = matrix('')  # Matrix of links visited count
        self.link_visit_count_matrix.fill(0)

        self.path_matrix_file = open('path_matrix_binary.txt', 'w+')
        # self.path_matrix_transpose_file = open('path_matrix_transpose.txt', 'w+')
        # self.path_matrix_dot_file = open('path_matrix_dot.txt', 'w+')
        # self.path_matrix_mul_file = open('path_matrix_mul.txt', 'w+')
        #
        # self.link_visit_count_matrix_file = open('link_visit_count_matrix.txt', 'w+')
        # self.link_visit_count_matrix_transpose_file = open('link_visit_count_matrix_transpose.txt', 'w+')
        # self.link_visit_count_matrix_dot_file = open('link_visit_count_matrix_dot.txt', 'w+')
        # self.link_visit_count_matrix_mul_file = open('link_visit_count_matrix_mul.txt', 'w+')

    def read_weights(self):
        pass

    def fill_links(self):
        # self.c.execute(self.get_edges)
        # edges = self.c.fetchall()
        #
        # self.c.execute(self.get_paths)
        # paths = self.c.fetchall()

        for src in xrange(1, self.files_count):
            self.c.execute(self.get_edges_by_id, {"src_id": src})
            edges_from = self.c.fetchall()
            self.edge_pair[src] = list(recipes.flatten(edges_from))

        # for row in edges:
        #     # row[0] id; row[1] src; row[2] dst; row[3] weight(visit count)
        #     print row
        #
        # for row in paths:
        #     # row[0] id; row[1] src; row[2] dst; row[3] weight(visit count)
        #     print row

    def create_path_matrix(self):
        sz = self.files_count
        self.path_binary_matrix = np.zeros((sz+1, sz+1), dtype=np.int)
        self.path_binary_matrix.setflags(align=True)
        self.path_binary_matrix[0] = np.arange(0, sz+1, 1)
        self.path_binary_matrix[:, 0] = np.arange(0, sz+1, 1)

        for row in range(1, sz):
            for col in range(1, sz):
                if col in self.edge_pair[row]:
                    self.path_binary_matrix[row][col] = 1
                    # self.path_binary_matrix_salt[row-1][col-1] = 1

        # self.path_binary_matrix_salt = np.zeros((sz, sz), dtype=np.int)

        # Save the original matrix
        np.savetxt(self.path_matrix_file, self.path_binary_matrix, fmt='%-8i', delimiter=' ')

        # matrix_org = self.path_binary_matrix

        # Save the transpose matrix
        # matrix_trans = matrix_org.transpose()
        # np.savetxt(self.path_matrix_transpose_file, matrix_trans, fmt='%-8i', delimiter=' ')

        # matrix_dot = np.dot(matrix_org, matrix_trans)
        # np.savetxt(self.path_matrix_dot_file, matrix_dot, fmt='%-8i', delimiter=' ')

        # matrix_mul = np.multiply(matrix_org, matrix_trans)
        # np.savetxt(self.path_matrix_mul_file, matrix_mul, fmt='%-8i', delimiter=' ')

        # self.link_visit_count_matrix = self.path_binary_matrix

        # for link in self.usages:
        #     self.link_visit_count_matrix[link[0], link[1]] += 1

        # np.savetxt(self.link_visit_count_matrix_file, self.link_visit_count_matrix, fmt='%-8i', delimiter=' ')

        # link_visit_count_matrix_transpose = self.link_visit_count_matrix.transpose()
        # np.savetxt(self.link_visit_count_matrix_transpose_file,
        #            link_visit_count_matrix_transpose, fmt='%-8i', delimiter=' ')

        # link_visit_matrix_mul = np.multiply(self.link_visit_count_matrix, link_visit_count_matrix_transpose)
        # np.savetxt(self.link_visit_count_matrix_mul_file, link_visit_matrix_mul, fmt='%-8i', delimiter=' ')
    def write_usages(self):
        self.page_link_exists.write("/\n")


if __name__ == "__main__":
    db_reader = DBReader()

    t0 = time.clock()

    print 'Started at.............: ' + str(datetime.now().time())

    db_reader.fill_links()
    # db_reader.create_path_matrix()

    print 'Process time(s)........: ', time.clock() - t0
    # print len(db_reader.edge_pair)
    # print db_reader.edge_pair[9]
    # for i in xrange(1, 21):
    #     print i, "--> ", db_reader.edge_pair[i]