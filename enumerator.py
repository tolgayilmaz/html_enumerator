'''

'''
__author__ = 'tolga'

import os, time, sqlite3
from datetime import datetime
from bs4 import BeautifulSoup
from numpy import matrix
from multiprocessing import Pool
from numpy import linalg
from random import randint
import numpy as np


class HTMLEnumerator(object):
    conn = sqlite3.connect('links.db')
    c = conn.cursor()

    # Defines the rules that an href contains to pass or parse deep
    rules=['.com','http','www','.net','com.us','.co.uk','.tr']

    def __init__(self):
        # Create tables
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS nodes (id INTEGER UNIQUE PRIMARY KEY, path TEXT)''')
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS edges
            (id INTEGER UNIQUE PRIMARY KEY, src INTEGER, dst INTEGER, weight INTEGER DEFAULT 0)''')
        self.c.execute(
            '''CREATE TABLE IF NOT EXISTS paths (id INTEGER UNIQUE PRIMARY KEY, session INTEGER, paths TEXT)''')

        self.insert_node = "INSERT INTO nodes VALUES (?,?)"
        self.insert_edge = "INSERT INTO edges VALUES (?,?,?,?)"
        self.update_flow = "UPDATE edges SET weight=:weight where src=:source and dst=:destination"
        self.insert_path = "INSERT INTO paths VALUES(?,?,?)"

        self.get_id = "SELECT id FROM nodes where path =:path"

        self.edge_pair = dict()             # A list of all links from key to other enumerated pages
        self.edge_pair.setdefault(0, [])    # page -> link1, link2, link3 ... {1,[3,4,6]}
        self.path_set = set()               # All html files in the site structure filled into this set
        self.path_map = dict()              # The html files from path_set enumerated regardless of any sequence

        self.usages = list()                # Collected usage data in format [source, destination]
        self.viewed_list = list()           # All viewed file enums to generate count of view size
        self.edge_weight = dict()           # Files mapped to view count
        self.edge_weight.setdefault((), 0)   # Weight of an edge
        self.page_view_count_map = dict()   # View count of all pages from usage data

        '''
        Files to be generated or to be used
        '''
        # Export all edges from parsed htmls in a format (page_from,page_to) row by row
        self.all_edges_file = open('edges.txt', 'w+')

        # Export all links between pages if exists
        self.page_link_exists = open('links_between.txt', 'w+')

        # Export all html files' view count from the usage files like (page_enum,view_count)
        self.page_view_count = open('views.txt', 'w+')

        # Export all links from a page like (source_page link1,link2,...linkn)
        self.links_file = open('links.txt', 'w+')

        # read usages from files separated with ||
        self.usages1 = open('m.9709.paths', 'r')
        self.usages2 = open('m.9710.paths', 'r')

        # After filling usage list export it like (from_page,to_page)
        self.usages_file = open('usages.txt', 'w+')

        self.trim_path   = os.path.join(os.path.dirname(__file__))+os.path.sep+'web_side'+os.path.sep
        self.search_path = os.path.join(os.path.dirname(__file__), 'web_side')
        self.file_count = 0

        print '.....system path    : ', self.trim_path
        print '.....searching path : ', self.search_path

    def fill_path_set(self):
        """
        :var search_path: Root directory for the files that will be searched for
        :var paths: output set of all the exact path(including filename) of htmls in the directory
        :return: paths in format [hyperreal.org/raven/index.html, hyperreal.org/index.html ...]
        """
        for root, directories, filenames in os.walk(self.search_path):
            for filename in filenames:
                if filename.endswith('html') or filename.endswith('htm'):
                    path = os.path.join(root, filename).replace(self.trim_path, '')
                    self.path_set.add(path.replace("\\", "/"))
        self.file_count = len(self.path_set)
        print "Total file count ......: ", self.file_count

    def enumerate_paths(self):
        """
        :var paths: The set containing all html file paths
        :var path_enum_map: map to be filled with id and html (index.html is 1)
        :return: fills path_enum_map like ['dir/to/index.html',1]
        """
        i = 1
        for path in self.path_set:
            self.path_map[path] = i
            self.c.execute(self.insert_node, [None, path])
            i += 1
        self.conn.commit()
        print "Path map filled ......... "

    def fill_html_links(self):
        """
        :param path_enum_map: html files to extract links from
        :return:edge pair list
        """
        for path, index in self.path_map.iteritems():
            # links = self.get_html_links(path)
            links = self.fetch_links(path)
            already_calculated = list()
            self.edge_pair[index] = list()
            for link in links:
                if link in self.path_map.keys():
                    pid = self.path_map[link]
                    edge = str(index) + ',' + str(pid)
                    self.edge_pair[index].append(pid)
                    if edge not in already_calculated:
                        self.all_edges_file.write(edge+'\n')
                        self.c.execute(self.insert_edge, [None, index, pid, 0])
                        already_calculated.append(edge)
        self.conn.commit()
        return self.edge_pair

    def fetch_links(self, html_path):
        """
        :param html_path: html file to be parsed for href links
        :return: return list of these links to other html files
        """
        links = []
        html_path = "web_side" + os.path.sep + html_path
        soup = BeautifulSoup(open(html_path, 'r'), "html.parser", from_encoding="iso-8859-1")

        # for link in soup.find_all(href=True):
        for link in soup.find_all('a'):
            href = link.get('href')

            if href is None:
                continue
            href = href.replace("http://", "")
            if href.endswith("/"):
                href += "index.html"
            if href in self.path_set:
                links.append(href)
        return links

    # def get_html_links(self, html_path):
    #     """
    #     :param html: html file to be parsed for href links
    #     :return: return list of these links to other html files
    #     """
    #     links = []
    #     soup = BeautifulSoup(open(html_path, 'r'), "html.parser", from_encoding="iso-8859-1")
    #
    #     for link in soup.find_all(href=True):
    #         href = link.get('href')
    #         if href is not None:  # and (href.endswith('.html') or href.endswith('.htm')):
    #             found_link_html = self.determine_file_from_link(html_path, href)
    #             if found_link_html in self.path_map.keys():
    #                 links.append(found_link_html)
    #     return links

    def determine_file_from_link(self, html_path, href):
        """
        :param html_path: the path to the html file to be parsed
        :param href: extracted link in html
        :return: path to link found in html
        """

        paths = html_path.split(os.path.sep)

        inlink = True
        for rule in self.rules:
            if href.find(rule) != -1:
                inlink = False

        href = href.replace('http://','')
        href = href.replace('www.','')
        up_count = href.count('../')

        # if link contains like ../../etc.html
        if up_count != 0:
            up_count += 1
            del paths[-up_count:]
            paths.extend(href.split('/')[up_count-1:])

        # if link is relative and links to html below current path i.e etc/etc.html
        elif inlink:
            del paths[-1]
            paths.extend(href.split('/'))
        else:
            # if link is referred via own sitename related to this site
            if href.find('hyperreal.org') == 0:
                paths = href.split('/')

            # if link is not related to this site
            else:
                paths = ''

        paths = os.path.sep.join(paths)
        return paths

    def write_links(self):
        for source, destinations in self.edge_pair.iteritems():
            self.links_file.write(str(source)+' ' + ','.join(str(x) for x in destinations)+'\n')

    def parse_usages(self):
        """
        A typical path will appear as below.
        The first line contains the originating machine (converted to unique numbers for the sake of anonymity).
        Each succeeding line corresponds to one URL requested from that machine.
        Each request contains the originating machine (O), the time of the request (T), the URL requested (U),
        and the referring URL (R). Fields are separated by "||".

        ---O:0000002560---
        O:0000002560 || T:1997/09/12-22:43:00 || U:/ || R:http://www.hyperreal.org/
        O:0000002560 || T:1997/09/12-22:50:27 || U:/categories/software/ || R:http://www.hyperreal.org/music/machines/
        O:0000002560 || T:1997/09/12-22:50:38 || U:/categories/software/Windows/ || R:http://www.hyperreal.org/music/machines/categories/software/
        O:0000002560 || T:1997/09/12-22:50:47 || U:/categories/software/Windows/V909V03.TXT || R:http://www.hyperreal.org/music/machines/categories/software/Windows/
        O:0000002560 || T:1997/09/12-22:51:06 || U:/categories/software/Windows/ || R:http://www.hyperreal.org/music/machines/categories/software/
        O:0000002560 || T:1997/09/12-22:51:18 || U:/categories/software/Windows/ravemusc.txt || R:http://www.hyperreal.org/music/machines/categories/software/Windows/
        """
        # TODO these two for loops can be parallelized
        session = 0
        path = []

        for row in self.usages1:
            path_len = len(path)
            if row.startswith("---"):
                if path_len > 0:
                    path_str = ','.join(str(e) for e in path)
                    self.c.execute(self.insert_path, [None, session, path_str])
                del path[:]
                new_session = True
                session = row.strip().strip("-").replace("O:", "").strip("0")
            else:
                new_session = False

            if not new_session:
                # parse rows and fill self.usages list
                visited = self.parse_row(row)
                try:
                    if path_len > 0 and path[-1] == visited[0]:
                        path.append(visited[1])
                    else:
                        path.append(visited[0])
                        path.append(visited[1])
                except TypeError:
                    continue
                except IndexError:
                    continue

        for row in self.usages2:
            path_len = len(path)
            if row.startswith("---"):
                if path_len > 0:
                    path_str = ','.join(str(e) for e in path)
                    self.c.execute(self.insert_path, [None, session, path_str])
                del path[:]
                new_session = True
                session = row.strip().strip("-").replace("O:", "").strip("0")
            else:
                new_session = False

            if not new_session:
                # parse rows and fill self.usages list
                visited = self.parse_row(row)
                try:
                    if path_len > 0 and path[-1] == visited[0]:
                        path.append(visited[1])
                    else:
                        path.append(visited[0])
                        path.append(visited[1])
                except TypeError:
                    continue
                except IndexError:
                    continue

        self.conn.commit()
        for usg in self.usages:
            self.usages_file.write(str(usg[0]) + ',' + str(usg[1]) + '\n')

    def parse_row(self, row):
        """
        :param session: The session number using the link
        :param row: string ro contains resurce and destination links seperated with ||
        :return: fills usages like [1,2],[34,543] numbers are enums to files
        """
        try:
            row_list    = row.split("||")
            # session     = row_list[0].strip().replace("O:", "").lstrip("0")
            # time        = row_list[1].strip().replace("T:", "")
            U           = row_list[2].strip().replace("U:", "").replace("http://", "")
            # truncate string from ? which is a parameter to html
            if U.find("?") != -1:
                U = U[0:U.find("?")]

            R = row_list[3].strip().replace("R:http://", ""). \
                replace("www.hyperreal.org/", "hyperreal.org/"). \
                replace("hyperreal.org/music/machines/", "machines.hyperreal.org/"). \
                replace("hyperreal.org/machines", "machines.hyperreal.org"). \
                replace("hyperreal.org/music", "music.hyperreal.org"). \
                replace("hyperreal.org/media", "media.hyperreal.org"). \
                replace("\\n", "")
        except IndexError:
            return

        if U == "/":
            root_R = R[0:R.find(".org/")+5]
            U = root_R+"index.html"
            # print U

        # What if link coming from a different web side
        if R.find("hyperreal.org") == -1:
            return

        if R == "R:-":
            R ="hyperreal.org/index.html"

        # truncate string from ? which may be a parameter to page
        if R.find("?") != -1:
            R = R[0:R.find("?")]
        if R.endswith("/") or not R.endswith(".html"):
            R += "index.html"

        first_path = U[0:U.find("/")+1]

        if R.find(first_path) != -1:
            U = R[0:R.find(first_path)] + U

        if U.endswith("/"):
            U += "index.html"

        try:
            dest = self.path_map[U]
            src = self.path_map[R]
            if src == dest:
                return
            self.usages.append([src, dest])
            self.viewed_list.append(dest)
        except KeyError:
            return
        return src, dest

    def calculate_weights(self):
        already_calculated = list()
        self.page_view_count.write("/\n")
        self.page_link_exists.write("/\n")
        for pair in self.usages:
            if pair not in already_calculated:
                weight = self.usages.count(pair)
                self.page_view_count.write(str(pair[0])+"."+str(pair[1])+" "+str(weight)+"\n")
                if weight > 0:
                    self.page_link_exists.write(str(pair[0])+"."+str(pair[1])+" "+str(1)+"\n")
                self.c.execute(self.update_flow, {"weight": weight, "source": pair[0], "destination": pair[1]})
                already_calculated.append(pair)
        self.page_view_count.write("/")
        self.page_link_exists.write("/")
        self.conn.commit()

    # Maybe GAMS will calculate this for us
    def write_page_view_counts(self):
        for page in range(1, len(self.path_set)):
            self.view_count_map[page] = self.viewed_list.count(page)

if __name__ == "__main__":
    enumerator = HTMLEnumerator()

    t0 = time.clock()
    print 'Started at.............: ' + str(datetime.now().time())

    enumerator.fill_path_set()
    enumerator.enumerate_paths()
    # # All html files are enumerated from now on
    # # TODO make it running parallel for available jobs
    # parallel = Pool()
    # parallel.apply(enumerator.fill_html_links)

    enumerator.fill_html_links()
    print "Edge database initialized"
    # At this point Nodes and edges are inserted into database

    enumerator.parse_usages()
    print "Usages analysed"

    enumerator.calculate_weights()
    print "Weights calculated"

    enumerator.write_links()
    # sz = 15
    # lss = range(1, 15)
    # path_binary_matrix = np.zeros((sz+1, sz+1), dtype=np.int)
    # path_binary_matrix.setflags(align=True)
    # path_binary_matrix[0] = np.arange(0,16,1)
    # path_binary_matrix[:,0] = np.arange(0,16,1)
    # # path_binary_matrix.reshape(sz, sz)
    #
    # for row in range(1, sz):
    #     for col in range(1, sz):
    #         # if col+1 in edge_pair[row+1]:
    #         path_binary_matrix[row][col] = randint(0, 1)
    # np.savetxt("test.txt", path_binary_matrix, fmt='%-8i', delimiter=' ')

    print 'Process time(s)........: ', time.clock() - t0
