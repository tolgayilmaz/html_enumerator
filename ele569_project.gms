$Title  Web Site Map Optimization
$Ontext

Bir web site yap�s� i�erisinde sayfalar�n birbirine olan ba��ml�l�klar�
ve kullan�m/g�sterim bilgileri toplanarak en �ok ziyaret edilen sayfalara kolay
ula��m�n sa�lanmas� ama�lanmakad�r.

Tracing within a web site structure and collecting linkage between pages;
we aim to optimize access to most viewed pages so that users of the web site
access more quickly pages they requested. As a result of this operation
bandwidth usage of the site may reduced.

Ayr�ca Generic Menu Optimization for Multi-profile Customer Systems makalesinin de
implementasyonu i�in kullan�labilir.

$Offtext

SET
   i web page ;

ALIAS(i,j)

PARAMETERS
         d(i,j)    Integer value denoting the weights of arcs. In general all weights are equal to 1 but the ones incoming to S_OPT are equal to 0
         L_in(i)   Constant that shows the maximum allowed number of incoming nodes to a particular node. i. /i*j/
         L_out(i)  Constant that shows the maximum allowed number of outgoing nodes from a particular node. i. /100/
         OUT(i)    Set of candidate outgoing nodes from ith node
         IN(i)     Set of candidate incoming nodes to ith node
         C(i)      Integer that shows click count for ith node.
         S_COMB(i) Set of Combiner menu items
         S_OPT(i)  Set of Optimizer menu items
         S(i)      Set of all menu nodes
         S_E(i)    Set of endpoint menu nodes that is leaf nodes
         S0(i)     Sink node the root level menu item.
         ;

VARIABLES
          Z
          a(i,j) Boolean variable which denotes if there exist a flow between the nodes i and j
          f(i,j) Amount of flow (click in this case) between menu nodes i and j
          ;

*is it ok to redefine a(i,j) here as binary?
BINARY VARIABLE
*1st Condition in the paper aij = {0,1}
                a(i,j) Boolean variable which denotes if there exist a flow between the nodes i and j
                ;
*is it ok to redefine f(i,j) here as positive?
POSITIVE VARIABLE
*2nd Condition in the paper fij >= 0
                f(i,j) Amount of flow (click in this case) between menu nodes i and j
                ;

SCALAR M Large constant with respect to given parameters in problem /1000/;

EQUATIONS
PAPER_COND3(i,j)
PAPER_COND4
PAPER_COND5
PAPER_COND6(i,j)
PAPER_COND7
PAPER_COND8
OBJECTIVE
;
OBJECTIVE .. Z =e= SUM(j, d(i,j)*f(i,j))
PAPER_COND4 .. f(i,j) =l= M*a(i,j)

;
*�rnek olarak 5*5 binary matrix denenebilir.
$call GDXXRW links.xlsx trace=3 par=d rng=Sheet1!b2 MaxDupeErrors=12 rdim=1 cdim=1 skipempty=1

$GDXIN       links.gdx
$LOAD        link
$GDXIN
Display link;


